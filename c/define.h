



/*
* Into this library you can find the definiton of some values
* LENGTH is the dimension of the array used into the comparative and not comparative
*   sorting algorithm that are based on an array
* MAX_RAND is used to define the range of numbers that will randomly fill the array
* MIN_RAND is used to define the range of numbers that will randomly fill the array
* the range is MIN_RAND <= random number <= MAX_RAND
* BASE is used into the radix sort algorithm, and is the base used to represent the number
* TABLE_LENGTH, DELETED and VOID are used into the hash table algorithms
* RED, BLACK and DBLACK are used by the red black tree algorithms
*/


#define LENGTH 50
#define MAX_RAND 50
#define MIN_RAND 0
#define ELEMENTS_FOR_ROW 15
#define BASE 10
#define TABLE_LENGTH 10
#define DELETED -1
#define VOID -2
#define RED 0
#define BLACK 1
#define DBLACK 2
