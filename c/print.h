
void array_print(int array[]);
void rbt_print(rbt* node);
void hash_chain_print(list* table[]);
void hash_open_print(int table[]);
void pre_print(bst *tree);
void in_print(bst *tree);
void post_print(bst *tree);
void list_print(list *l);

void array_print(int array[]){
    int i;
    for(i = 0; i < LENGTH; i += 1)
    if((i +1) % ELEMENTS_FOR_ROW)
    if((i + 1) % ELEMENTS_FOR_ROW)
      printf ("%8d", array[i]);
    else
      printf ("%8d\n", array[i]);
  printf("\n");
}

void rbt_print(rbt *node){
    if(node != NULL){
        rbt_print(node->left);
        printf("value: %d,",node->key);
        if(node->color)
            printf(" color: BLACK\n");
        else
            printf(" color: RED\n");
        rbt_print(node->right);
    }
    else
        printf("LEAF, color: BLACK\n");
}

void hash_chain_print(list* table[]){
  int i;
  for(i = 0; i < TABLE_LENGTH; i += 1){
    printf ("Position %d", i);
    list_print(table[i]);
    printf("\n");
  }
}

void hash_open_print(int table[]){
  int i;
  for (i = 0; i < TABLE_LENGTH; i += 1){
    if(table[i] > 0)
      printf("%8d", table[i]);
    else
      if(table[i] == VOID)
        printf(" VOID");
      else
        printf(" DELETED");
    if((i + 1) % ELEMENTS_FOR_ROW == 0)
      printf("\n");
  }
  printf("\n");
}

void pre_print(bst *tree){
  if(tree != NULL){
    printf("%8d",tree->key);
    pre_print(tree->left);
    pre_print(tree->right);
  }
}

void in_print(bst *tree){
  if(tree != NULL){
    in_print(tree->left);
    printf("%8d",tree->key);
    in_print(tree->right);
  }
}

void post_print(bst *tree){
  if(tree != NULL){
    post_print(tree->left);
    post_print(tree->right);
    printf("%8d",tree->key);
  }
}

void list_print(list *lis){
  list* l = lis;
  while(l != NULL){
    printf("%8d", l->value);
    l = l->next;
  }
}