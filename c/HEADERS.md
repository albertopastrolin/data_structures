Description of the Various Headers
==================================

A quick description of the various header
  
* define.h
  In this header you can find the definition of various macro

  
* list.h
  In this header you can find the implementation of a list, and some
  functions to manipulate it

* hash.h
  In this header you can find the implementation of a chaining hash table
  and the implementation of a open adressing hash table; also you can find
  some functions to manipulate them
 
* tree.h
  In this header you can find the implementation of a BST, and some
  functions to manipulate it
  
* rbtree.h
  In this header you can find the implementation of a RBT, and some
  functions to manipulate it

* print.h
  In this header you can find the functions used to print the various data
  structures