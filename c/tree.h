

/*
 * Implementation of a binary search tree and various function to manipulate it;
 * to use it you also need this header:
 * define.h
 */


struct bstree{
  int key;
  struct bstree *left;
  struct bstree *right;
  struct bstree *parent;
};
typedef struct bstree bst;


bst* tree_add(bst *tree, bst *par, int val);
bst* tree_delete(bst* tree, int val);
bst* tree_search(bst *tree, int val);
bst* search_max(bst *tree);
bst* search_min(bst *tree);
bst* search_pre(bst *tree);
bst* search_suc(bst *tree);
int pre_array(bst *tree, int array[], int i);
int post_array(bst *tree, int array[], int i);
int in_array(bst *tree, int array[], int i);


bst* tree_add(bst *tree, bst *par, int val){
  if(tree == NULL){
    tree = (bst*) malloc(sizeof(bst));
    tree->key = val;
    tree->left = NULL;
    tree->right = NULL;
    tree->parent = par;
  }
  else
    if(val < tree->key)
      tree->left = tree_add(tree->left, tree, val);
    else
      tree->right = tree_add(tree->right, tree, val);
  return tree;
}

bst* tree_search(bst *tree, int val){
  if(tree == NULL || tree->key == val)
    return tree;
  else
    if(val < tree->key)
      return tree_search(tree->left, val);
    else
      return tree_search(tree->right, val);
}

bst* search_max(bst *tree){
  if(tree == NULL || tree->right == NULL)
    return tree;
  else
    return search_max(tree->right);
}

bst* search_min(bst *tree){
  if(tree == NULL || tree->left == NULL)
    return tree;
  else
    return search_min(tree->left);
}

bst* search_pre(bst *tree){
  if(tree->left != NULL)
    return search_max(tree->left);
  bst *y = tree->parent;
  while(y != NULL && y == tree->left){
    tree = y;
    y = y->parent;
  }
  return y;
}

bst* search_suc(bst *tree){
  if(tree->right != NULL)
    return search_min(tree->right);
  bst *y = tree->parent;
  while(y != NULL && y == tree->right){
    tree = y;
    y = y->parent;
  }
  return y;
}

int pre_array(bst *tree, int array[], int i){
  if(tree != NULL){
    array[i] = tree->key;
    i += 1;
    i = pre_array(tree->left, array, i);
    i = pre_array(tree->right, array, i);
  }
  return i;
}

int post_array(bst *tree, int array[], int i){
  if(tree != NULL){
    i = post_array(tree->left, array, i);
    i = post_array(tree->right, array, i);
    array[i] = tree->key;
    i += 1;
  }
  return i;
}

int in_array(bst *tree, int array[], int i){
  if(tree != NULL){
    i = in_array(tree->left, array, i);
    array[i] = tree->key;
    i += 1;
    i = in_array(tree->right, array, i);
  }
  return i;
}

bst* tree_delete(bst* tree, int val){
  if(tree != NULL)
    if (val < tree->key)
      tree->left = tree_delete(tree->left, val);
    else
      if (val > tree->key)
        tree->right = tree_delete(tree->right, val);
      else
        if (tree->left == NULL){
          bst *tmp = tree->right;
          free(tree);
          tree = tmp;
        }
        else
          if(tree->right == NULL){
            bst *tmp = tree->left;
            free(tree);
            tree = tmp;
          }
          else{
            bst *tmp = search_suc(tree->right), *par = tmp->parent;
            if (par != NULL)
              par->left = tree_delete(par->left, par->left->key);
            else
              tree->right = tree_delete(tree->right, tree->right->key);
          }
  return tree;
}
