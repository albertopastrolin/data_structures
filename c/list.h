

/*
 * Implementation of a list and various function to manipulate it;
 * to use it you also need this header:
 * define.h
 */


struct list{
  int value;
  struct list *next;
};
typedef struct list list;


list* add(list *l, int v);
list* delete(list *l, int v);
int search(list* l, int v);


list* add(list *l, int v){
  if(l == NULL){
    l = (list*) malloc(sizeof(list));
    l->next = NULL;
    l->value = v;
  }
  else
    l->next = add( l->next, v);
  return l;
}

list* delete(list* l, int v){
  if(l != NULL)
    if(l->value == v){
      list* tmp = l->next;
      free(l);
      l = tmp;
    }
    else
      l->next = delete(l->next, v);
  return l;
}

int search(list* l, int v){
  if(l == NULL)
    return -1;
  else
    if(l->value == v)
      return 0;
    else
      return 1 + search(l->next, v);
}
