

/*
 * Implementation of an hash table and various function to manipulate it;
 * to use it you also need this header:
 * define.h
 * list.h
 */


//Function for the chaining hash table
void hash_chain_initialize(list* table[]);
void hash_chain_insert(list* table[], int value);
void hash_chain_delete(list* table[], int value);
int hash_chain_search(list* table[], int value);

//Function for the open adressing hash table
void hash_open_initalize(int table[]);
void hash_open_insert(int table[], int value);
void hash_open_delete(int table[], int value);
int hash_open_search(int table[], int value);

//Hash function
int hash(int key);
int hash_with_scan(int key, int retry);

//Start of the chaining hash table function
void hash_chain_insert(list* table[], int value){
  table[hash(value)] = add(table[hash(value)], value);
}

void hash_chain_delete(list* table[], int value){
  table[hash(value)] = delete(table[hash(value)], value);
}

int hash_chain_search(list* table[], int value){
  return search(table[hash(value)], value);
}

void hash_chain_initialize(list* table[]){
  int i;
  for (i = 0; i < TABLE_LENGTH; i += 1)
    table[i] = 	NULL;
}
//End of the chaining hash table function

//Start of the open adressing hash table function
void hash_open_insert(int table[], int value){
  int i = 0;
  while (i < TABLE_LENGTH && table[hash_with_scan(value, i)] != VOID)
    i += 1;
  if (i < TABLE_LENGTH)
    table[hash_with_scan(value, i)] = value;
  else
    printf("full!!!\n");
}

void hash_open_delete(int table[], int value){
  int d = hash_open_search(table, value);
  if(d >= 0)
    table[d] = DELETED;
}

int hash_open_search(int table[], int value){
  int i = 0;
  while( i < TABLE_LENGTH && table[hash_with_scan(value, i)] != VOID && 
        table[hash_with_scan(value,i)] != value && table[hash_with_scan(value, i)] != value)
    i += 1;
  if(i == TABLE_LENGTH || table[hash_with_scan(value, i)] == VOID || table[hash_with_scan(value, i)] == DELETED)
    return -1;
  else
    return i;
}

void hash_open_initalize(int table[]){
  int i;
  for (i = 0; i < TABLE_LENGTH; i += 1)
    table[i] = VOID;
}
//End of the open adressing hash table function

//A simple Hash function, in this case the module
int hash(int key){
  return ( key % TABLE_LENGTH );
}

int hash_with_scan(int key, int retry){
  return ( (hash(key) + retry) % TABLE_LENGTH );
}
