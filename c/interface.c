

/*
 * This is a quick interface for testing purpose
 * I have tested it only under GNU/Linux, but probably it will work
 * on every unix-like system, so under GNU/Linux, BSD and Mac OS X it
 * should work (let me know if not); under Windows probably it won't work,
 * if I remember correctly the time library needs some adjustment, so try it
 * and if it works, ok, else fix it, your problem ^-^.
 * Ah, if you use an "exotic" OS (eg openIndiana), if it is POSIX compliant
 * probably this interface will work, if not it same of Windows, if it works ok,
 * else fix it.
*/


#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "define.h"
#include "list.h"
#include "hash.h"
#include "tree.h"
#include "rbtree.h"
#include "print.h"


void menu();
void hash_function();
void open_hash();
void chain_hash();
void tree_function();
void rbtree_function();


int main(){
    menu();
    return 0;
}

void menu(){
    int choice;
    do{
        printf ("0) Exit\n");
        printf ("1) hash_function\n");
        printf ("2) tree_function\n");
        printf ("3) rbtree_function\n");
        scanf ("%d", &choice);
        switch (choice){
      case 1:
        hash_function();
      break;
      case 2:
        tree_function();
      break;
      case 3:
        rbtree_function();
      break;
        }
    }
    while(choice);
}

void hash_function(){
  list* chain_table[TABLE_LENGTH];
  int open_table[TABLE_LENGTH], choice, value;
    long start_time, end_time;
    double work_time;
    do{
        printf ("0) Back\n");
        printf ("1) chain_hash\n");
        printf ("2) open_hash\n");
        scanf ("%d", &choice);
        switch (choice){
            case 1:
                chain_hash();
            break;
            case 2:
                open_hash();
            break;
        }
    }
    while(choice);
}

void open_hash(){
  int open_table[TABLE_LENGTH], choice, value;
    long start_time, end_time;
    double work_time;
    do{
        printf ("0) Back\n");
        printf ("1) hash_open_initialize\n");
        printf ("2) hash_open_print\n");
        printf ("3) hash_open_insert\n");
        printf ("4) hash_open_delete\n");
        printf ("5) hash_open_search\n");
        scanf ("%d", &choice);
        switch (choice){
            case 1:
                hash_open_initalize(open_table);
            break;
            case 2:
        hash_open_print(open_table);
            break;
            case 3:
        printf("Insert a value: ");
        scanf("%d", &value);
        printf("\n");
                start_time = clock();
                hash_open_insert(open_table, value);
                end_time = clock();
                work_time = (double) (end_time - start_time) / CLOCKS_PER_SEC;
                printf ("Added in %5.10f seconds\n", work_time);
            break;
            case 4:
        printf("Insert a value: ");
        scanf("%d", &value);
        printf("\n");
                start_time = clock();
                hash_open_delete(open_table, value);
                end_time = clock();
                work_time = (double) (end_time - start_time) / CLOCKS_PER_SEC;
                printf ("Deleted in %5.10f seconds\n", work_time);
            break;
            case 5:
        printf("Insert a value: ");
        scanf("%d", &value);
        printf("\n");
                start_time = clock();
                value = hash_open_search(open_table, value);
                end_time = clock();
                work_time = (double) (end_time - start_time) / CLOCKS_PER_SEC;
                printf ("Searched in %5.10f seconds, founded at position %d\n", work_time, value);
            break;
        }
    }
    while(choice);
}

void chain_hash(){
  list* chain_table[TABLE_LENGTH];
  int choice, value;
    long start_time, end_time;
    double work_time;
    do{
        printf ("0) Back\n");
        printf ("1) hash_chain_initialize\n");
        printf ("2) hash_chain_print\n");
        printf ("3) hash_chain_insert\n");
        printf ("4) hash_chain_delete\n");
        printf ("5) hash_chain_search\n");
        scanf ("%d", &choice);
        switch (choice){
            case 1:
                hash_chain_initialize(chain_table);
            break;
            case 2:
        hash_chain_print(chain_table);
      break;
            case 3:
        printf("Insert a value: ");
        scanf("%d", &value);
        printf("\n");
        start_time = clock();
        hash_chain_insert(chain_table, value);
                end_time = clock();
                work_time = (double) (end_time - start_time) / CLOCKS_PER_SEC;
                printf ("Added in %5.10f seconds\n", work_time);
            break;
            case 4:
        printf("Insert a value: ");
        scanf("%d", &value);
        printf("\n");
        start_time = clock();
        hash_chain_delete(chain_table, value);
                end_time = clock();
                work_time = (double) (end_time - start_time) / CLOCKS_PER_SEC;
                printf ("Deleted in %5.10f seconds\n", work_time);
            break;
            case 5:
        printf("Insert a value: ");
        scanf("%d", &value);
        printf("\n");
                start_time = clock();
                value = hash_chain_search(chain_table, value);
                end_time = clock();
                work_time = (double) (end_time - start_time) / CLOCKS_PER_SEC;
                printf ("Searched in %5.10f seconds, founded at position %d\n", work_time, value);
            break;
        }
    }
    while(choice);
}

void tree_function(){
  int choice, value, array[LENGTH];
    long start_time, end_time;
    double work_time;
    bst *root = NULL, *ret;
    do{
        printf ("0) Back\n");
        printf ("1) add\n");
        printf ("2) search\n");
        printf ("3) search_min\n");
        printf ("4) search_max\n");
        printf ("5) search_pre\n");
        printf ("6) search_suc\n");
        printf ("7) pre_print\n");
        printf ("8) in_print\n");
        printf ("9) post_print\n");
        printf ("10) pre_array\n");
        printf ("11) in_array\n");
        printf ("12) post_array\n");
        printf ("13) tree_delete\n");
        scanf ("%d", &choice);
        switch (choice){
            case 1:
        printf("Insert a value: ");
        scanf("%d", &value);
        printf("\n");
        root = tree_add(root, NULL, value);
            break;
            case 2:
        printf("Insert a value: ");
        scanf("%d", &value);
        printf("\n");
                start_time = clock();
                ret = (int) tree_search(root, value);
                end_time = clock();
                work_time = (double) (end_time - start_time) / CLOCKS_PER_SEC;
                printf ("Searched in %5.10f seconds, founded at the adress %d\n", work_time, ret);
            break;
            case 3:
        start_time = clock();
        ret = search_min(root);
                end_time = clock();
                work_time = (double) (end_time - start_time) / CLOCKS_PER_SEC;
                printf ("Searched in %5.10f seconds, founded at the adress %d\n", work_time, ret);
      break;
            case 4:
                start_time = clock();
                ret = search_max(root);
                end_time = clock();
                work_time = (double) (end_time - start_time) / CLOCKS_PER_SEC;
                printf ("Searched in %5.10f seconds, founded at the adress %d\n", work_time, ret);
            break;
            case 5:
                start_time = clock();
                ret = search_pre(root);
                end_time = clock();
                work_time = (double) (end_time - start_time) / CLOCKS_PER_SEC;
                printf ("Searched in %5.10f seconds, founded at the adress %d\n", work_time, ret);
            break;
            case 6:
                start_time = clock();
                ret = search_suc(root);
                end_time = clock();
                work_time = (double) (end_time - start_time) / CLOCKS_PER_SEC;
                printf ("Searched in %5.10f seconds, founded at the adress %d\n", work_time, ret);
            break;
            case 7:
                pre_print(root);
            break;
            case 8:
                in_print(root);
            break;
            case 9:
        post_print(root);
            break;
            case 10:
        pre_array(root, array, 0);
        array_print(array);
            break;
            case 11:
        in_array(root, array, 0);
        array_print(array);
            break;
            case 12:
        post_array(root, array, 0);
        array_print(array);
            break;
            case 13:
        printf("Insert a value: ");
        scanf("%d", &value);
        printf("\n");
        root = tree_delete(root, value);
            break;
        }
        printf("\n");
    }
    while(choice);
}

void rbtree_function(){
  int choice, value;
    rbt *root = NULL;
    do{
        printf ("0) Back\n");
        printf ("1) add\n");
        printf ("2) print\n");
        scanf ("%d", &choice);
        switch (choice){
            case 1:
        printf("Insert a value: ");
        scanf("%d", &value);
        printf("\n");
        root = rbtree_add(root, NULL, value);
            break;
            case 2:
                rbt_print(root);
            break;
        }
        printf("\n");
    }
    while(choice);
}
