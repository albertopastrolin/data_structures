

/*
 * Implementation of a red black tree and various function to manipulate it;
 * to use it you also need this header:
 * define.h
 */

struct rbtree{
  int key;
    int color;
  struct rbtree *left;
  struct rbtree *right;
  struct rbtree *parent;
};
typedef struct rbtree rbt;

rbt* right_rotate(rbt* root, rbt* node);
rbt* left_rotate(rbt* root, rbt* node);
rbt* rbtree_add(rbt* node, rbt* par, int value);
rbt* correction(rbt* node);
rbt* granny(rbt* node);
rbt* sam(rbt* node);

rbt* granny(rbt* node){
    if(node != NULL && node->parent != NULL)
        return node->parent->parent;
    else
        return NULL;
}

rbt* sam(rbt* node){
    rbt *gramps = granny(node);
    if (gramps == NULL)
        return NULL;
    else
        if (gramps->right == node->parent)
            return gramps->left;
        else
            return gramps->right;
}

rbt* right_rotate(rbt* root, rbt* node){
    if(node->left != NULL){
        rbt *lef = node->left, *par = node->parent;
        lef->parent = par;
        if(par != NULL)
            if(node == par->left)
                par->left = lef;
            else
                par->right = lef;
        else
            root = lef;
        node->left = lef->right;
        node->left->parent = node;
        lef->right = node;
        node->parent = lef;
    }
    return root;
}

rbt* left_rotate(rbt* root, rbt* node){
    if(node->right != NULL){
        rbt *rig = node->right, *par = node->parent;
        rig->parent = par;
        if(par != NULL)
            if(node == par->right)
                par->right = rig;
            else
                par->left = rig;
        else
            root = rig;
        node->right = rig->left;
        node->right->parent = node;
        rig->left = node;
        node->parent = rig;
    }
    return root;
}

rbt* rbtree_add(rbt* node, rbt* par, int value){
    if(node == NULL){
        node = (rbt*) malloc(sizeof(rbt));
        node->parent = par;
        node->left = NULL;
        node->right = NULL;
        node->key = value;
        node->color = RED;
        node = correction(node);
    }
    else
        if(value < node->key)
            node->left = rbtree_add(node->left, node, value);
        else
            node->right = rbtree_add(node->right, node, value);
    return node;
}

rbt* correction(rbt* node){
    rbt *par = node->parent;
    if(par == NULL){
        node->color = BLACK;
        return node;
    }
    if(par->color == BLACK)
        return node;
    else{
        rbt *gramp = granny(node), *uncle = sam(node);
        if(uncle != NULL && uncle->color == RED){
            par->color = BLACK;
            uncle->color = BLACK;
            gramp->color = RED;
            gramp = correction(gramp);
        }
        else
            if(node == par->right && uncle == gramp->right){
                gramp = left_rotate(gramp, node);
                node = node->left;
            }
            else
                if(node == par->left && uncle == gramp->left){
                    gramp = right_rotate(gramp, node);
                    node = node->right;
                }
                else
                    if(node == par->right && uncle == gramp->left)
                        gramp = left_rotate(gramp, node);
                    else
                        gramp = right_rotate(gramp, node);
        return node;
    }
}
