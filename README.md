data_structures
==================
Implementation fo various data structures

How to use this code:

A) The correct way

Set up git (https://help.github.com/articles/set-up-git) and simply do

git clone https://github.com/iSamuraii/data_structures

to initialize the repository and pull the source. If you want to update the code simply do

git pull

B) The wrong way

Open the file you want to save, select all, CTRL+C, and CTRL+V into your favourite editor (remeber to save the file with the correct name).
