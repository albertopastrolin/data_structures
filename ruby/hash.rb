def hash(key)
  return key % TABLE_LENGTH
end

def hash_with_scan(key, ret)
  return (hash(key) + ret) % TABLE_LENGTH
end

def chain_hash_insert(table, key)
  table[hash(key)] << key
end

def chain_hash_delete(table, key)
  table[hash(key)].delete(key)
end

def chain_hash_search(table, key)
  i = 0
  arr = table[hash(key)]
  while i < arr.length && arr[i] != key
    i += 1
  end
  if i < arr.length
    return i
  else
    return -1
  end
end

def open_hash_insert(table, key)
  i = 0
  while i < TABLE_LENGTH && table[hash_with_scan(key, i)] != "VOID"
    i += 1
  end
  if i < TABLE_LENGTH
    table[hash_with_scan(key,i)] = key
  else
    puts "FULL"
  end
end

def open_hash_delete(table, key)
  d = open_hash_search(table, key)
  if d > 0
    table[d] = "DELETED"
  end
end

def open_hash_search(table, key)
  i = 0
  hws = hash_with_scan(key, i)
  while i < TABLE_LENGTH && table[hws] != "VOID" && table[hws] != "DELETED" && table[hws] != key
    i += 1
    hws = hash_with_scan(key, i)
  end
  if i == TABLE_LENGTH || table[hws] == "VOID" || table[hws] == "DELETED"
    return -1
  else
    return i
  end
end
