require "benchmark"
require "./define.rb"
require "./hash.rb"

def menu()	
  begin
    puts "0) Exit"
    puts "1) hash_function"
    choice = gets.to_i
    case choice
      when 1
        hash_function()
      when 0
        puts "Exit..."
      else
        puts "Unsupported Option"
        redo
    end
  end while choice > 0
end

def hash_function()	
  begin
    puts "0) Back"
    puts "1) chain_hash_function"
    puts "2) open_hash_function"
    choice = gets.to_i
    case choice
      when 1
        chain_hash_function()
      when 2
        open_hash_function()
      when 0
        puts "Back..."
      else
        puts "Unsupported Option"
        redo
    end
  end while choice > 0
end

def chain_hash_function()	
  begin
    puts "0) Back"
    puts "1) chain_hash_initialize"
    puts "2) chain_hash_print"
    puts "3) chain_hash_insert"
    puts "4) chain_hash_delete"
    puts "5) chain_hash_search"
    choice = gets.to_i
    case choice
      when 1
        chain_hash = Array.new(TABLE_LENGTH) {Array.new()}
      when 2
        p chain_hash
      when 3
        puts "Insert a value"
        value = gets.to_i
        chain_hash_insert(chain_hash, value)
      when 4
        puts "Insert a value"
        value = gets.to_i
        chain_hash_delete(chain_hash, value)
      when 5
        puts "Insert a value"
        value = gets.to_i
        index = chain_hash_search(chain_hash, value)
        puts "Finded " + value.to_s + " at location "+ index.to_s + " of the " + (hash(value) + 1).to_s + " list"
      when 0
        puts "Back..."
      else
        puts "Unsupported Option"
        redo
    end
  end while choice > 0
end

def open_hash_function()	
  begin
    puts "0) Back"
    puts "1) open_hash_initialize"
    puts "2) open_hash_print"
    puts "3) open_hash_insert"
    puts "4) open_hash_delete"
    puts "5) open_hash_search"
    choice = gets.to_i
    case choice
      when 1
        open_hash = Array.new(TABLE_LENGTH, "VOID")
      when 2
        p open_hash
      when 3
        puts "Insert a value"
        value = gets.to_i
        open_hash_insert(open_hash, value)
      when 4
        puts "Insert a value"
        value = gets.to_i
        open_hash_delete(open_hash, value)
      when 5
        puts "Insert a value"
        value = gets.to_i
        index = open_hash_search(open_hash, value)
        puts "Finded " + value.to_s + " at location "+ index.to_s
      when 0
        puts "Back..."
      else
        puts "Unsupported Option"
        redo
    end
  end while choice > 0
end

menu()
